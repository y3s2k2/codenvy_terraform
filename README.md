# クラウドIDE環境でterraformを利用

クラウドIDE環境はcodenvyを利用します

# terraformバージョン管理ソフト

https://github.com/Zordrak/tfenv

# terraformの環境構築

1. Codenvyで空のworkspaceを作る
2. コマンドを実行する
```
$ git clone https://gitlab.com/y3s2k2/codenvy_terraform.git
$ cd codenvy_terraform
$ . setup.sh
```
3. 指定したバージョンをインストール/アンインストール
```
$ tfenv install 0.8.0
$ tfenv install latest # latest version
$ tfenv install latest:^0.8 # latest version with Regex
$ tfenv uninstall 0.8.0
```

# 事前準備

## AWS

### IAMユーザの作成

任意の名前でユーザを作成してください。設定は以下の通りです。

アクセスの種類：「プログラムによるアクセス」
アクセス権限：「既存のポリシーを直接アタッチ」
ポリシー名：「AdministratorAccess」

### S3バケットの作成

AWS S3にtfstate格納用のbucketを作成します。 S3 bucket名は任意ですが、S3 bacuket名は世界でただ1つだけしか名前を付けられない(重複不可である)ため、
ご自身で作成した頂く必要があります。その他S3 bucketの設定はデフォルトでOKです。 

### キーペアの発行
EC2インスタンス接続用の鍵であるキーペアを作成します。 キーペアはEC2インスタンスのメニューから作成できます。キーペア名は任意です。


## Local

### gitインストール

Mac
```
$ brew install git
$ git --version
```

Windows
```
https://git-for-windows.github.io/ よりインストーラをダウンロードして実行する。
```

インストール完了後コマンドプロンプトから以下のコマンドを実行し、バージョンが表示されればOK
> git --version

### 設定ファイルの変更

IAM作成後、terraform.tfvars をご自身で作成したアクセスキー、シークレットキーに変更してください。


設定例)
```
aws_access_key_id = アクセスキー
aws_secret_access_key = シークレットキー
```

S3 bucket作成後、backend.tfのbucketを ご自身で作成したS3 bucket名に変更してください。
```

terraform {
  backend "s3" {
    bucket = "ご自身で作成したS3 bucket名"
    region = "ap-northeast-1"
    key    = "任意の名称/terraform.tfstate"
  }
}
```

### キーペアの発行

キーペア作成後、variable.tfのkey_pairをご自身で作成したキーペア名に変更してください。

```
variable "key_pair" {
  default = "ご自身で作成したキーペア名"
}
```
