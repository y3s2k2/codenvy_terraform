variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "region" {
    default = "ap-northeast-1"
}

# キーペア名をdefaultに設定してください。
variable "key_pair" {
    default = "awsec2key"
}

#
# vpc
#
variable "vpc_settings" {
  type = "map"
  default = {
    enable_dns_hostnames  = true
    enable_dns_support  = true
    cidr = "10.0.0.0/16"
  }
}

#
# subnet
#
variable "public_subnet_length" {
  default = "2"
}
variable "az_list" {
    default = "ap-northeast-1a,ap-northeast-1c"
}
variable "public_subnet_settings" {
  type = "map"
  default = {
    subnet_cidr_16 = "10.0"
    map_public_ip_on_launch = "true"
  }
}

#
# ec2
#
variable "aws_zone" {
    default = "ap-northeast-1c"
}
variable "images" {
    type = "map"
    default = {
        Amazon-Linux-2 =  "ami-04d3eb2e1993f679b"
        Amazon-Linux = "ami-06cd52961ce9f0d85"
        Windows-Server-2016 = "ami-0134dae8964ea643c"
        Windows-Server-2012R2= "ami-0a45b66bda01c89c8"
    }
}

variable "web_settings" {
  type = "map"
  default = {
    ec2_count = "2"
    ec2_type = "t2.micro"
    # ami_id = "${lookup(var.images, "Amazon-Linux")}"
  }
}

#
# RDS
#
variable "rds_settings" {
  type = "map"
  default = {
    instance_class = "db.t2.micro"
    database_name = "wordpress"
    db_username = "wordpress"
  }
}

#
# Password
#
variable "password_length" {
  default = "10"
}

#
# S3
#
variable "s3_bucket" {
    default = "goto-wordpress"
}

#
# Route 53
#
# variable "route53_zone" {
#     default = "jenelab-hands-on.com"
# }
# variable "route53_zoneid" {
#     default = "Z3HEA6RWGTOE56"
# }
# variable "route53_record" {
#     default = "www.jenelab-hands-on.com"
# }