#!/bin/bash
cd /tmp
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm

# パッケージインストール
sudo yum update -y
sudo yum install -y httpd

# html作成
echo '<h2>Welcome to AZ_A</h2>' > /var/www/html/index.html

# Apacheの自動起動設定
sudo chkconfig httpd on

# Apacheの再起動
sudo service httpd restart
